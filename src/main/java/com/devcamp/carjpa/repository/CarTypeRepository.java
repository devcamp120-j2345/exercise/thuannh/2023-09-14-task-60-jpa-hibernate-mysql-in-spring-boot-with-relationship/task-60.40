package com.devcamp.carjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.carjpa.model.CarType;

public interface CarTypeRepository extends JpaRepository<CarType, Long> {
    
}
