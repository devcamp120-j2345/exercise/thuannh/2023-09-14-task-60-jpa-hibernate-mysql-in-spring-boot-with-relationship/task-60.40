package com.devcamp.carjpa.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.carjpa.model.Car;

public interface CarRepository extends JpaRepository<Car, Long>  {
    Car findByCarCode(String carCode);
}

