package com.devcamp.carjpa.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.carjpa.model.Car;
import com.devcamp.carjpa.model.CarType;
import com.devcamp.carjpa.repository.CarRepository;

public class CarController {
        @Autowired
    private CarRepository carRepository;

    @GetMapping("/devcamp-cars")
    public ResponseEntity<List<Car>> getAllCars() {
        try {
            List<Car> allCars = new ArrayList<>();

            carRepository.findAll().forEach(allCars::add);

            return new ResponseEntity<>(allCars, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-cartypes")
public ResponseEntity<List<CarType>> getCarTypeByCarCode(@RequestParam(value = "carCode") String carCode) {
    try {
        Car vCar = carRepository.findByCarCode(carCode);
        if (vCar != null) {
            List<CarType> carTypes = new ArrayList<>();
            carTypes.addAll(vCar.getCarType());
            return new ResponseEntity<>(carTypes, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

}
